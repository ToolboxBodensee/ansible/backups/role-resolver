Configure resolv.conf
=====================

Ansible role to configure the `/etc/resolv.conf` file


Variables
---------

* `resolver_nameservers`:
  List of name servers.

* `resolver_searchdomains`:
  List of search domains.
